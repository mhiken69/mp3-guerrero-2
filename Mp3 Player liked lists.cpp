#include <iostream>
#include <conio.h>
using namespace std;
class Node {
  public:    string data;
    	    Node *next;
             Node() {};
             void SetData(string aData) { data = aData; };
             void SetNext(Node* aNext) { next = aNext; };
             string Data() { return data; };
             Node* Next() { return next; };
};

class List {
      Node *head;
      public:
             List() { head = NULL; };
             void Display();
             void Append(string data);
             void Remove(string data);
           	 string ViewData(int index);             
};

string List::ViewData(int index){
	Node* temp	=	head;
	int	Index	=	1;
	while (Index != index){
		temp =	temp->next;
		Index++;
	}
	if (temp) return temp->data;
	return NULL;
}


void List::Display() {
	int num = 0;
	Node* temp	=	head;
	while (temp != NULL){
		cout<<num+1<<"."<<temp->data<<endl;
		temp	=	temp->next;
		num++;
	}
	
}

void List::Append(string data) {
     Node* newNode = new Node();
     newNode->SetData(data);
     newNode->SetNext(NULL);
     Node *temp = head;
     if ( temp != NULL ) {
          while ( temp->Next() != NULL ) {
                temp = temp->Next();
          }
     temp->SetNext(newNode);
     }
     else {
          head = newNode;
     }
}

void List::Remove(string data) {
     Node *temp = head;
     if ( temp == NULL )
          return;
     if ( temp->Next() == NULL ) {
          delete temp;
          head = NULL;
     }
     else {
          Node *prev;
          do {
              if ( temp->Data() == data ) break;
              prev = temp;
              temp = temp->Next();
          } while ( temp != NULL );
     prev->SetNext(temp->Next());
     delete temp;
     }
}

int main(){
	List list, lib;
	char choice;
	string music;
	int i;
	int x;
	
	lib.Append("Dun dun dun durundurun dun dun");
	lib.Append("What is love");
	lib.Append("baby don't hurt me");
	lib.Append("don't hurt me, no more");
	
	

	Main_menu:
	cout <<"\n\n\nAdd chosen music to the Playlist: :A: "<<endl;
	cout <<"Remove music from the playlist :B:"<< endl;
	cout <<"Display Playlist: :C:" <<endl;
	cout <<"Remove Data from the library: :D:"<< endl;
	cout <<"View Library list: :E:"<< endl; 	
	cout <<"Add music to Library :F:" << endl;
	cout <<"Edit music data(cannot edit built-in music): :G:" << endl;
	cin>> choice;
	
	            if (choice=='A'||choice=='a'){
	             			lib.Display();			
                			cout<<"\nEnter the corespoding number of that song you wish to add: \n";
                                        cin>> i;
                                         	list.Append(lib.ViewData(i));
											cout<< "Playlist:"<<endl;						
                                        	list.Display();
}
                  
  				if (choice=='D'||choice=='d'){  
                            lib.Display();
							cout<<"\nMusic to be removed(title): ";
                            cin>> music;
                            lib.Remove(music);
                            
}
				if (choice == 'C' || choice== 'c'){
					
					cout <<"\nSong- you want to play: "<<endl;
					list.Display();
					cin>> i;
					cout<<"Now playing: "<<lib.ViewData(i)<<endl;	
									
				}
				
				if (choice == 'E' || choice == 'e'){
					
					cout << "Song Library: " << endl;
					lib.Display();
					
					
				}
				
				if (choice == 'G' || choice == 'g'){
					system ("cls");
					lib.Display();
					cout<< "Music you want to edit(title): "<< endl;
					cin >> music;
					lib.Remove(music);
					cout << "Input new title:";
					cin >> music;
					lib.Append(music);
					lib.Display();
					
				}
				if (choice == 'F' || choice == 'f'){
										
                                        cout<<"Please Enter the Title of the music : "<<endl;
										cin.ignore();
										getline(cin,music);
										lib.Append(music);
				}
				if (choice=='B'||choice=='b'){  
                            lib.Display();
							cout<<"\nMusic to be removed(title): ";
                            cin>> music;
                            lib.Remove(music);
							 }
				
	cout<< "\n\n\nPress any key to go back.";
		getch();
		goto Main_menu;
}
